package com.example.amlservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Row")
@XmlAccessorType(XmlAccessType.FIELD)
public class Row {

    private String item;
    private String value;
    private String message;
    private String suspect;

    @XmlElement(name = "spc_reason")
    private String spcReason;

    @XmlElement(name = "full_suspect")
    private String fullSuspect;

}
