package com.example.amlservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Description")
@XmlAccessorType(XmlAccessType.FIELD)
public class Description {


    @XmlElement(name = "Row")
    private List<Row> rows;

    @XmlAttribute(name = "num")
    private String num;
}
