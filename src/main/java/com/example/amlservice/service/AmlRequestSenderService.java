package com.example.amlservice.service;

import com.example.amlservice.model.AmlCheckResult;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

public class AmlRequestSenderService {


    private String filePath = "templates/amlTemplate.xml";
    private static final String PERSONAL_DATA_HOLDER = "PERSONAL_DATA";
    private static final String ADDRESS_HOLDER = "ADDRESS";
    private static final String REQUEST_URL = "http://localhost:9001/itwGateWS/exec/FISPut";
    private static final String UTF_ENCODING = "UTF-8";
    private static final String EXPECTED_RESULT_FOR_AML = "<ReturnCode>0</ReturnCode>";

    public boolean getAmlResult(String addressData, String personalData) {
        try {
            InputStream resourceStream = AmlRequestSenderService.class.getClassLoader().getResourceAsStream(filePath);
            StringWriter writer = new StringWriter();
            IOUtils.copy(resourceStream, writer, UTF_ENCODING);
            String xmlRequest = writer.toString().replace(ADDRESS_HOLDER, addressData).replace(PERSONAL_DATA_HOLDER, personalData);
            String requestResult = sendRequest(xmlRequest);
            return requestResult.contains(EXPECTED_RESULT_FOR_AML);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private String sendRequest(String data) throws IOException {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpPost httpPost = new HttpPost(REQUEST_URL);
            httpPost.setEntity(new StringEntity(data, UTF_ENCODING));
            httpPost.setHeader("Content-Type", "text/xml");

            try (CloseableHttpResponse httpResponse = httpClient.execute(httpPost)) {
                return EntityUtils.toString(httpResponse.getEntity());
            }
        }
    }

    public void parseResponse(String responseString) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        Document doc = docBuilder.parse(new InputSource(new StringReader(responseString)));

        NodeList childNodes = doc.getElementsByTagName("Description").item(0).getChildNodes();
        NodeList nodes = ((Node) childNodes).getChildNodes();

        AmlCheckResult amlCheckData = new AmlCheckResult();
        int bound = nodes.getLength();
        for (int i = 0; i < bound; i++) {
            if ("PEOPLE_FULL_NAME".equals(nodes.item(i).getFirstChild().getTextContent())) {
                String textContent = nodes.item(i).getLastChild().getTextContent();
                amlCheckData.getFullNameData().add(textContent);
            } else if ("PERSON_ADDRESS".equals(nodes.item(i).getFirstChild().getTextContent())) {
                String textContent = nodes.item(i).getLastChild().getTextContent();
                amlCheckData.getAddressData().add(textContent);
            }
        }

        amlCheckData.getFullNameData().forEach(System.out::println);
        amlCheckData.getAddressData().forEach(System.out::println);
    }
}
